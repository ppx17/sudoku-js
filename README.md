Javascript implementation of a simple backtracking Sudoku solver, that can also generate new puzzles and validate your progress. 

Live demo: https://ppx17.gitlab.io/sudoku-js
