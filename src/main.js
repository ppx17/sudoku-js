function main() {
    const domElement = document.getElementById('game');

    const generator = new Generator();

    const btnNewGame = document.getElementById('btnNewGame');

    const newBoard = () => {
        domElement.classList.add("generating");
        btnNewGame.value = "Generating...";
        btnNewGame.disabled = true;
        generator.generate()
            .then(applyBoard)
            .finally(() => {
                domElement.classList.remove("generating");
                btnNewGame.value = "New game";
                btnNewGame.disabled = false;
            })
    }

    const applyBoard = (board) => {
        domElement.innerHTML = '';
        new Renderer(domElement, board).render();

        const solver = new Solver();

        const confirmAction = (action) => {
            if (!board.isInProgress() || board.isFinished()) {
                return true;
            }

            return window.confirm('Are you sure you want to ' + action + '? You seem to have a game in progress and you will lose that progress.');
        }

        btnNewGame.onclick = () => {
            if (!confirmAction("start a new game")) {
                return;
            }
            newBoard();
        }

        const lblStatus = document.getElementById('lblStatus');
        const btnSolve = document.getElementById('btnSolve');
        btnSolve.onclick = () => {
            if (!confirmAction('solve this puzzle')) {
                return;
            }
            const solvable = solver.solve(board, new IncreasingOptions());
            lblStatus.textContent = (solvable ? 'Solved' : 'This puzzle is unsolvable.');
        }

        const btnValidate = document.getElementById('btnValidate');
        btnValidate.onclick = () => {
            lblStatus.textContent = (board.isSolvable()
                ? (board.isFinished()
                    ? "You've solved the puzzle correctly!"
                    : "You're still on track!")
                : "You've made an error");
        };

        const btnReset = document.getElementById('btnReset');
        btnReset.onclick = () => {
            if (!confirmAction('remove all your input')) {
                return;
            }
            board.clearFilled();
        }

        const btnHighlightMistakes = document.getElementById('btnHighlightMistakes');
        btnHighlightMistakes.onclick = () => {
            board.forAllErrors(s => s.markInvalid());
        }

        const btnRemoveMistakes = document.getElementById('btnRemoveMistakes');
        btnRemoveMistakes.onclick = () => {
            board.forAllErrors(s => s.setValue(null))
        }

        board.onFinished = () => {
            domElement.classList.add("finished");
            setTimeout(() => domElement.classList.remove("finished"), 6000);
        }

        window.addEventListener('keydown', (e) => {
            const focused = board.focusedSpace();
            if (focused === undefined) return;
            let y = focused.y;
            let x = focused.x;
            if (e.key === "ArrowUp") {
                if (y === 0) return;
                y--;
            } else if (e.key === "ArrowDown") {
                y++;
            } else if (e.key === "ArrowLeft") {
                if (x === 0) return;
                x--;
            } else if (e.key === "ArrowRight") {
                x++;
            }
            const newSpace = board.board[y][x];
            if (newSpace === undefined || !newSpace.hasInput()) return;
            newSpace.input.focus();
            newSpace.input.select();
        })

        window.addEventListener('beforeunload', (e) => {
            if(!board.isInProgress() || board.isFinished()) {
                return;
            }

            e.preventDefault();
            e.returnValue = '';
        })
    }

    newBoard();
}

class Space {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.value = null;
        this.fixed = false;
    }

    setBoard(board) {
        this.board = board;
    }

    clone() {
        const c = new Space(this.x, this.y);
        c.value = this.value;
        c.fixed = this.fixed;
        return c;
    }

    hasValue() {
        return this.value !== null;
    }

    setInput(input) {
        this.input = input;
        this.input.addEventListener('input', this.onInput.bind(this));
        this.input.addEventListener('focus', this.onFocus.bind(this));
        this.input.readOnly = this.fixed;
        this.input.value = this.value;
    }

    markFixed() {
        this.fixed = true;
        if (this.hasInput()) {
            this.input.readOnly = true;
        }
    }

    markNotFixed() {
        this.fixed = false;
        if (this.hasInput()) {
            this.input.readOnly = false;
        }
    }

    markInvalid(mark = true) {
        this.setClass("invalid", mark);
    }

    highlight(mark = true) {
        this.setClass("highlight", mark);
    }

    setClass(cssClass, active) {
        if (!this.hasInput()) return;
        if (active) {
            this.input.classList.add(cssClass);
        } else {
            this.input.classList.remove(cssClass);
        }
    }

    isFixed() {
        return this.fixed;
    }

    onInput(e) {
        this.setValue(e.target.value);
    }

    onFocus() {
        if (this.board !== undefined) {
            this.board.onSpaceFocused(this);
        }
    }

    onFilled() {
        if (this.board !== undefined) {
            this.board.onSpaceFilled();
        }
    }

    reset() {
        this.markNotFixed();
        this.setValue(null);
    }

    setValue(number) {
        if (this.isFixed()) {
            console.error(`Attempting to update ${this.x}:${this.y} which was marked as fixed`);
            return;
        }
        this.markInvalid(false);
        const nr = Number(number);
        if (nr >= 1 && nr <= 9) {
            this.value = nr;
            this.setInputValue(nr);
            this.onFilled();
            return;
        }

        this.value = null;
        this.setInputValue("");
    }

    setInputValue(value) {
        if (this.hasInput()) {
            this.input.value = value;
        }
    }

    hasInput() {
        return this.input !== undefined;
    }

    getValue() {
        return this.value;
    }
}

class Board {
    constructor(generateSpaces = true) {
        this.width = 9;
        this.height = 9;
        this.board = [];
        this.spaces = [];
        this.onFinished = null;
        for (let y = 0; y < this.height; y++) {
            this.board.push([]);
            if (generateSpaces) {
                for (let x = 0; x < this.width; x++) {
                    let space = new Space(x, y);
                    space.setBoard(this);
                    this.board[y].push(space);
                    this.spaces.push(space);
                }
            }
        }
    }

    clone() {
        const clone = new Board(false);
        for (let row of this.rows) {
            for (let s of row) {
                let clonedSpace = s.clone();
                clone.board[s.y][s.x] = clonedSpace;
                clone.spaces.push(clonedSpace);
            }
        }
        return clone;
    }

    solution() {
        const answer = this.clone();
        answer.clearFilled();
        this.solver().solve(answer, new IncreasingOptions());
        return answer;
    }

    isSolvable() {
        return this.solver().solve(this.clone(), new IncreasingOptions());
    }

    isFinished() {
        return this.allSpacesFilled() && this.isSolvable();
    }

    allSpacesFilled() {
        return this.spaces.filter(s => !s.hasValue()).length === 0;
    }

    forAllErrors(callback) {
        if (this.isSolvable()) return;
        const solution = this.solution()
        this.spaces
            .filter(s => !s.isFixed() && s.hasValue())
            .forEach(space => {
                const expected = solution.board[space.y][space.x].getValue();
                if (space.getValue() !== expected) {
                    callback(space, expected);
                }
            });
    }

    get rows() {
        return this.board;
    }

    row(y) {
        return this.board[y];
    }

    col(x) {
        return this.board.map(r => r[x]);
    }

    square(x, y) {
        let result = [];
        for (let iy of this.squareAxis(y)) {
            for (let ix of this.squareAxis(x)) {
                result.push(this.board[iy][ix]);
            }
        }
        return result;
    }

    squareAxis(i) {
        if (i < 3) return [0, 1, 2];
        if (i < 6) return [3, 4, 5];
        return [6, 7, 8];
    }

    clearFilled() {
        this.spaces.forEach(space => {
            if (!space.isFixed() && space.hasValue()) {
                space.setValue(null);
            }
        })
    }

    reset() {
        this.spaces.forEach(space => space.reset());
    }

    focusedSpace() {
        return this.spaces.find(s => s.hasInput() && s.input === document.activeElement);
    }

    isInProgress() {
        return this.spaces.find(s => !s.isFixed() && s.hasValue()) !== undefined;
    }

    solver() {
        if (this._solver === undefined) {
            this._solver = new Solver();
        }
        return this._solver;
    }

    highlightValue(value) {
        this.spaces.forEach(s => s.highlight(s.getValue() === value && value !== null));
    }

    onSpaceFocused(space) {
        this.highlightValue(space.getValue());
    }

    onSpaceFilled() {
        const isFinished = this.isFinished();
        if (this.onFinished !== null && isFinished) {
            this.onFinished(this);
        }
    }
}

class Solver {
    hasSingleSolution(board) {
        const inc = board.clone();
        const dec = board.clone();

        if (!this.solve(inc, new IncreasingOptions()) || !this.solve(dec, new DecreasingOptions())) {
            return false;
        }

        for (let space of inc.spaces) {
            if (space.getValue() !== dec.board[space.y][space.x].getValue()) {
                return false;
            }
        }

        return true;
    }

    solve(board, options) {
        const empty = board.spaces.find(s => !s.hasValue());
        if (empty === undefined) {
            return true;
        }

        for (let i of options.options()) {
            if (this.isLegit(board, empty, i)) {
                empty.setValue(i);
                if (this.solve(board, options)) {
                    return true;
                } else {
                    empty.setValue(null);
                }
            }
        }
        return false;
    }

    isLegit(board, space, number) {
        return board.col(space.x).find(s => s.getValue() === number) === undefined
            && board.row(space.y).find(s => s.getValue() === number) === undefined
            && board.square(space.x, space.y).find(s => s.getValue() === number) === undefined;
    }
}

class IncreasingOptions {
    options() {
        return [1, 2, 3, 4, 5, 6, 7, 8, 9];
    }
}

class DecreasingOptions {
    options() {
        return [9, 8, 7, 6, 5, 4, 3, 2, 1];
    }
}

class RandomOptions {
    constructor() {
        this._options = new IncreasingOptions();
    }

    options() {
        return this.shuffle(this.shuffle(this._options.options()));
    }

    shuffle(arr) {
        return arr.sort(() => Math.random() - 0.5);
    }
}

class Renderer {
    constructor(container, board) {
        this.container = container;
        this.board = board;
    }

    render() {
        const table = document.createElement('table');
        table.className = "sudoku";
        const body = table.createTBody();
        this.board.rows.forEach(row => {
            const tr = body.insertRow();
            row.forEach(space => {
                const td = tr.insertCell();
                const input = document.createElement('input');
                input.maxLength = 1;
                input.onclick = (e) => e.target.select();
                space.setInput(input);
                td.appendChild(input);
            })
        });
        this.container.appendChild(table);
    }
}

class Generator {
    async generate() {
        return new Promise((ok) => {
            setTimeout(() => {
                const board = new Board();
                const solver = new Solver();

                solver.solve(board, new RandomOptions());

                for (let i = 0; i < 60; i++) {
                    let space = this.pickRandomFilledSpace(board);
                    let pickedValue = space.getValue();
                    space.setValue(null);

                    if (!this.validateBoard(solver, board, 5)) {
                        space.setValue(pickedValue);
                    }
                }

                board.spaces.forEach(s => {
                    if (s.hasValue()) {
                        s.markFixed();
                    }
                });

                ok(board);
            }, 50);
        })
    }

    validateBoard(solver, board) {
        return solver.hasSingleSolution(board.clone());
    }

    pickRandomFilledSpace(board) {
        const options = this.shuffle(this.shuffle(board.spaces.filter(s => s.hasValue())));

        return options[0];
    }

    shuffle(arr) {
        return arr.sort(() => Math.random() - 0.5);
    }
}

window.onload = main;
